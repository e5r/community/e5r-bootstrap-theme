The E5R Bootstrap Theme
=======================

> The E5R Development Team web site theme for Bootstrap v4

Tema Bootstrap para sites do Time de Desenvolvimento E5R.

## Desenvolvendo

* NodeJS >= 9.0

```console
$ npm install
```

### Gerando o CSS do tema

```console
$ mkdir dist
$ npx sass theme/e5r-bootstrap.scss dist/e5r-bootstrap-theme.css
```

### Veja os exemplos no navegador

```console
$ npx serve
   ┌───────────────────────────────────────────────────┐
   │                                                   │
   │   Serving!                                        │
   │                                                   │
   │   - Local:            http://localhost:5000       │
   │   - On Your Network:  http://xxx.xxx.xxx.x:5000   │
   │                                                   │
   │   Copied local address to clipboard!              │
   │                                                   │
   └───────────────────────────────────────────────────┘
```

Abra o navegador em http://localhost:5000/samples.
